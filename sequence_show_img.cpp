#include "sequence_show_img.h"
#include "ui_sequence_show_img.h"

sequence_show_img::sequence_show_img(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::sequence_show_img)
{
    ui->setupUi(this);
    //连接slot
    connect(ui -> last, SIGNAL(clicked()), this, SLOT(last_show()));
    connect(ui -> next, SIGNAL(clicked()), this, SLOT(next_show()));
    connect(ui -> radioslider, SIGNAL(valueChanged(int)),this, SLOT(redrawpic(int)));
}

sequence_show_img::~sequence_show_img()
{
    delete ui;
}
//显示上张图片
void sequence_show_img::last_show(){
    idx = idx + int(datas.size()) - 1;
    idx %= int(datas.size());
    loadpic();
}
//显示下张图片
void sequence_show_img::next_show(){
    idx = idx + 1;
    idx %= int(datas.size());
    loadpic();
}

//载入图片
void sequence_show_img::loadpic(){
    ui -> num_label->setText(QString("%1 / %2").arg(QString::number(idx + 1), QString::number(int(datas.size()))));
    img = QImage(width, height, QImage::Format_RGB32);
    for(int j = 0; j < height; ++j)
       for(int k = 0; k < width; ++k){
            int num = j * width + k;
            if(filter_value == -1){
                //no filter show
                img.setPixel(k,j,qRgb(datas[idx][3 * num + 2],datas[idx][3 * num + 1],datas[idx][3 * num + 0]));
            }
            else{
                // filter show
                if(datas[idx][3 * num + 2] < int(filter_value * 255)){
                    img.setPixel(k,j,qRgb(0,0,0));//black
                }
                else{
                    img.setPixel(k,j,qRgb(datas[idx][3 * num + 2],datas[idx][3 * num + 1],datas[idx][3 * num + 0]));
                }
            }
        }
    //设置缩放显示到label
    ui -> img_label ->setPixmap(QPixmap::fromImage(img).scaled(width * zoom_radio, height * zoom_radio,Qt::KeepAspectRatio));
    //debug img
    /*
        cout << "save img" << endl;
        img.save("/Users/gsl/Desktop/result.jpg");
   */
}

//改变 radio 重新draw picture
void sequence_show_img::redrawpic(int pos){
    //set zoom radio
    zoom_radio = 1.0 * (pos + 100) / 100;
    //redraw picture
    loadpic();
}
