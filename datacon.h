#ifndef DATACON_H
#define DATACON_H
#include <iostream>
#include <string>
#include "QTSource.h"
#include "config.h"
using namespace std;
// pid pname pdir pwicount dwicount modifytime
struct Patient_information{
    string id;
    string name;
    QDateTime checktime;
    QDateTime importtime;
    void debug(){
        cout << id << " " << name << " " << checktime.toString().toStdString() << " " <<  importtime.toString().toStdString() << endl;
    }
};
class DataCon{
public:
    DataCon();
    static QSqlDatabase db;
    string tablename;
    static map <string, DataCon *> map_db;
    static DataCon * getinstance(string str){
        if(map_db.find(str) == map_db.end()){
            return NULL;
        }
        return map_db[str];
    }
    static void init(){
        db = QSqlDatabase::addDatabase(Config::single()->mp["db_type"], "default");
        db.setDatabaseName(Config::single()->mp["db_name"]);
        if (!db.open()){
            qDebug() << "Failed to connect to root mysql admin";
        }
        else{
            qDebug() << "connect to database";
        }
    }

    static void insert(string type, string table_name){
        QSqlQuery q(("create table " + table_name + "(pid varchar(255), pname varchar(255), checktime varchar(255), importtime varchar(255));").c_str(),db);
        auto res = new DataCon(table_name);
        map_db[type] = res;
    }

    vector <Patient_information> getall(){
        QSqlQuery q(("select pid,pname,checktime,importtime from " + tablename).c_str(), db);
        vector <Patient_information> res;
        while(q.next()){
            Patient_information tmp;
            tmp.id = q.value(0).toString().toStdString();
            tmp.name = q.value(1).toString().toStdString();
            tmp.checktime = QDateTime::fromString(q.value(2).toString(),Qt::TextDate);
            tmp.importtime = QDateTime::fromString(q.value(2).toString(),Qt::TextDate);
            res.push_back(tmp);
        } 
        printf("%d\n", res.size());
        return res;
    }
    void add(const char * pid, const char * pname, QString checktime, QString importtime){
        QSqlQuery q(("insert into " + tablename + "(pid,pname,checktime,importtime) values(?,?,?,?)").c_str(), db);
        q.addBindValue(pid);
        q.addBindValue(pname);
        q.addBindValue(checktime);
        q.addBindValue(importtime);
        q.exec();
    }
    vector <Patient_information> likely_search(string id, string name){
        QSqlQuery q(("select pid,pname,checktime, importtime from " + tablename + " where pid like '%" + id + "%' and pname like '%" + name + "%'").c_str(), db);
        vector <Patient_information> res;
        while(q.next()){
            Patient_information tmp;
            tmp.id = q.value(0).toString().toStdString();
            tmp.name = q.value(1).toString().toStdString();
            tmp.checktime = QDateTime::fromString(q.value(2).toString(),Qt::TextDate);
            tmp.importtime = QDateTime::fromString(q.value(3).toString(),Qt::TextDate);
            res.push_back(tmp);
        }
        return res;
    }
private:
    DataCon(string _tablename):tablename(_tablename){

    }
};

#endif // DATACON_H
