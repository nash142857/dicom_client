#include "displaypwi.h"
#include "ui_displaypwi.h"
#include "DCMHeaders.h"
#include "DCMConst.h"
#include "config.h"
DisplayPWI::DisplayPWI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DisplayPWI)
{
    ui->setupUi(this);
    ui -> slider -> setMinimum(0);
    ui -> slider -> setMaximum(100);
    lastshow = Config::single()->mp["brain_pdata_dir"];
    connect(ui->pwiselect, SIGNAL(clicked()), this, SLOT(display()));
    connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(changeshow(int)));
    connect(ui->save, SIGNAL(clicked()), this, SLOT(savepic()));
    pwi_list.clear();
}

DisplayPWI::~DisplayPWI()
{
    //delete
    if(!pwi_list.empty()){
        //delete last show
        for(int i = 0; i < pwi_list.size();++i)
            delete pwi_list[i];
    }
    delete ui;

}

void DisplayPWI::display(){
    printf("display\n");
    if(!pwi_list.empty()){
        //delete last show
        for(int i = 0; i < pwi_list.size();++i)
            delete pwi_list[i];
    }
    pwi_list.clear();
    QFileDialog fileDialog(this);
    fileDialog.setWindowTitle(tr("打开 图片"));
    fileDialog.setDirectory(Config::single()->mp["brain_pdata_dir"]);//self define
    fileDialog.setFilter(QDir::Dirs);
    if(fileDialog.exec() == QDialog::Accepted) {
        QString path = fileDialog.selectedFiles()[0];
        QDir dir(path);
        dir.setFilter(QDir::Files);
        QFileInfoList list = dir.entryInfoList();
        int total = list.count();
        for(int i = 0; i < total; ++i){
            DcmFileFormat *pDicomFile = new DcmFileFormat();
            OFCondition oc = pDicomFile->loadFile((path + QChar('/') + list.at(i).fileName()).toStdString().c_str());
            if(oc.good()){
                DcmDataset *pDataset = pDicomFile->getDataset();
                pwi_list.push_back(new ImageMatrix(pDataset));
            }
            //delete dicom file reader
            delete pDicomFile;
        }
        lastshow = path;
        if(pwi_list.empty()) return;
        int width = pwi_list[0] -> getColumns();
        int height = pwi_list[0] -> getRows();
        vector < int * > data;
        for(int  i = 0; i < pwi_list.size(); ++i){
            data.push_back(pwi_list[i] -> display(10000, 0, 100000));
        }
        ui -> pwi_show -> set_para(data, width, height);
        ui -> pwi_show -> repaint();
    }else{
        QMessageBox::information(NULL, tr("Path"), tr("You didn't select any files."));
    }
}

void DisplayPWI::changeshow(int value){
    ui -> pwi_show -> set_filter_value_radio(value * 1.0 / 100);
    ui -> pwi_show -> repaint();
}
void DisplayPWI::savepic(){
    QMessageBox msgbox(this);
    msgbox.addButton(tr("否"), QMessageBox::AcceptRole);
    msgbox.addButton(tr("是"), QMessageBox::RejectRole);
    msgbox.setText(tr("你想保存PWI图片么"));
    if(msgbox.exec() == QMessageBox::Accepted){
        QFileDialog fileDialog(this);
        fileDialog.setWindowTitle(tr("保存图片"));
        fileDialog.setDirectory(lastshow.toStdString().c_str());//self define
        fileDialog.setFilter(QDir::Dirs);
        if(fileDialog.exec() == QDialog::Accepted){
            QString path = fileDialog.selectedFiles()[0];
            QDir dir(path);
            ui -> pwi_show -> img.save(path + QString("/result.jpg"));//save picture
            QMessageBox::about(this,"",tr("保存成功"));
        }
    }
}
