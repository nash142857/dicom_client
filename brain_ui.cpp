#include "brain_ui.h"
#include "ui_brain_ui.h"
#include "datacon.h"
#include "search_patient.h"
#include "displaypwi.h"
#include "setpara.h"
brain_ui::brain_ui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::brain_ui)
{
    patient_search = NULL;
    set_pa = NULL;
    displaypwi = NULL;
    ui->setupUi(this);
    setLayout(ui->verticalLayout);
    patient_search = new search_patient("brain");
    set_pa = new SetPara(); 
    displaypwi = new DisplayPWI();
    ui->tabWidget->addTab(patient_search, "查找");
    ui->tabWidget->addTab(set_pa, "参数设置");
    ui->tabWidget->addTab(displaypwi, "PWI过滤");
    connect(ui -> tabWidget, SIGNAL(currentChanged(int)), this, SLOT(load(int)));
    resize(1000,700);
}
void brain_ui::load(int i){
    if(i == 1){
        set_pa -> load_from_memory();
    }
}

brain_ui::~brain_ui()
{
    //delete sub widget
    if(patient_search != NULL) delete patient_search;
    if(set_pa != NULL) delete set_pa;
    if(displaypwi != NULL) delete displaypwi;
    delete ui;
}
