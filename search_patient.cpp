#include "search_patient.h"
#include "ui_search_patient.h"
//ui_search_patient from this know the widget inside a widget
#include "common_util.h"
#include "dcmpatientinfo.h"
#include "config.h"
#include "vtk.h"
#include "pre_calculate.h"
#include <QProcess>
//show pid pname modifytime
search_patient::search_patient(const QString & _data_type, QWidget *parent) :
    data_type(_data_type),QWidget(parent),
    ui(new Ui::search_patient){
    ui->setupUi(this);
    setLayout(ui->verticalLayout);
    ui->patient_list->setColumnCount(4);
    ui->patient_list->setRowCount(20);
    QStringList header;
    header << "ID" << "姓名" << "修改时间" << "检查时间";
    ui->patient_list->setHorizontalHeaderLabels(header);
    ui->patient_list->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->patient_list->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    //ui->patient_list->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->patient_list->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->patient_list->setSelectionBehavior(QAbstractItemView::SelectRows);
    auto res = DataCon::getinstance(data_type.toStdString()) -> getall();
    setdata(res);
    connect(ui -> patient_list->horizontalHeader(), SIGNAL(sectionDoubleClicked(int)), this, SLOT(sort(int)));
    //dynamic connect
    if(data_type == "brain"){
        connect(ui -> patient_list, &QTableWidget::doubleClicked, [=](const QModelIndex  & model_idx){
            int idx = model_idx.row();
            //return if not set
            if(ui->patient_list->item(idx,0) == NULL) return;
            QString	id = ui -> patient_list -> item(idx,0)->text();
            auto pre_cal = new pre_calculate(id);
            pre_cal->setWindowTitle("ID:" + id + " 版本控制界面");
            pre_cal -> show();
        });
    }
    else if(data_type == "vessel"){
        connect(ui->patient_list,&QTableWidget::doubleClicked, [=](const QModelIndex & model_idx){
            int idx = model_idx.row();
            //return if not set
            if(ui->patient_list->item(idx,0) == NULL) return;
            QString	id = ui -> patient_list -> item(idx,0)->text();
            //3d vessel file
            auto group3d_file = Config::single()->mp["vessel_pdata_dir"] + "/" + id + "/group.vtp";
            //check file exists
            if(!QFile(group3d_file).exists()){
                QMessageBox::about(this,"","数据不存在");
                return;
            }
            else{
                vtk::show_vtp_file(group3d_file);
            }
        });
    }
    connect(ui -> search, SIGNAL(clicked()), this, SLOT(search()));
}

search_patient::~search_patient(){
    delete ui;
}
void search_patient::sort(int idx){
    Qt::SortOrder order[] = {Qt::DescendingOrder, Qt::AscendingOrder};
    static int now_order_idx = 0;
    ui->patient_list->sortItems(idx, order[now_order_idx]);
    now_order_idx = 1 - now_order_idx;
}

void search_patient::setdata(vector <Patient_information> & data){
    ui -> patient_list -> clear();
    QStringList header;
    header << "ID" << "姓名" << "修改时间" << "检查时间";
    ui->patient_list->setHorizontalHeaderLabels(header);
    for(int i = 0; i < data.size(); ++i){
        ui->patient_list->setItem(i,0,new QTableWidgetItem(data[i].id.c_str()));
        ui->patient_list->setItem(i,1,new QTableWidgetItem(data[i].name.c_str()));
        ui->patient_list->setItem(i,2,new QTableWidgetItem(data[i].checktime.toString("yyyy-MM-dd hh:mm:ss")));
        ui->patient_list->setItem(i,3,new QTableWidgetItem(data[i].importtime.toString("yyyy-MM-dd hh:mm:ss")));
    }
}

void search_patient::search(){
    auto res = DataCon::getinstance(data_type.toStdString()) -> likely_search(ui -> input_id -> text().toStdString(),ui -> input_name -> text().toStdString());
    setdata(res);
}
