#include "net_util.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include "QsLog.h"
#include <utility>
QString net_util::base_url = "http://127.0.0.1:8080";
QString net_util::get_version_url = net_util::base_url + "/getallversion";
QString net_util::rename_version_url  = net_util::base_url + "/rename";
QString net_util::get_file_url = net_util::base_url + "/getfile";
QString net_util::tcp_host = "127.0.0.1";
QString net_util::delete_version_url = net_util::base_url + "/delete";
int net_util::tcp_port = 8888;
bool net_util::get(QString url, QString & res){
    QNetworkRequest req;
    QEventLoop loop;
    QNetworkAccessManager net_access;
    req.setUrl(QUrl(url));
    QNetworkReply * reply = net_access.get(req);
    QObject::connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    QObject::connect(reply, SIGNAL(finished()), reply,SLOT(deleteLater()));
    loop.exec();
    if(reply->error() == QNetworkReply::NoError){
        res = reply->readAll();
        return true;
    }
    else{
        QLOG_ERROR() << "get data error:" << url;
        res = "";
        return false;
    }
}
bool net_util::post(QString url, const map <QString, QString> & mp, QString & res){
    QNetworkRequest req;
    QEventLoop loop;
    QNetworkAccessManager net_access;
    req.setUrl(QUrl(url));
    QString str = "";
    bool flg = false;
    for(map<QString,QString>::const_iterator itr = mp.begin(); itr != mp.end(); ++itr){
        if(!flg){
            str += itr->first + "=" + itr->second;
            flg = true;
        }
        else{
            str += "&" + itr->first + "=" + itr->second;
        }
    }
    QNetworkReply * reply = net_access.post(req,str.toUtf8());
    QObject::connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    QObject::connect(reply, SIGNAL(finished()), reply,SLOT(deleteLater()));
    loop.exec();
    if(reply->error() == QNetworkReply::NoError){
        res = reply->readAll();
        return true;
    }
    else{
        QLOG_ERROR() << "get data error:" << url;
        res = "";
        return false;
    }
}
