#ifndef DISPLAYPATIENT_H
#define DISPLAYPATIENT_H

#include <QWidget>
#include "QTSource.h"
#include "dcmpatientinfo.h"
#include "show_img.h"
extern const char * temp_filename;
namespace Ui {
class DisplayPatient;
}

class DisplayPatient : public QWidget
{
    Q_OBJECT

public:
    explicit DisplayPatient(QWidget *parent = 0);
    ~DisplayPatient();
    void set_inf(DCMPatientInfo * data, string _version = temp_filename);
private slots:
    void show_all(int a);
    void save_data();
private:
    DCMPatientInfo *patient;
    show_img * dwi_display;
    //MTT Tmax CBV CBF
    show_img * other_pic_disp[4];
    show_img * all_disp;
    QScrollArea * scrollshow;
    Ui::DisplayPatient *ui;
    vector <string> show_label;
    vector <int *> all_datas;
    string version;
};

#endif // DISPLAYPATIENT_H
