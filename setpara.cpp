#include "setpara.h"
#include "ui_setpara.h"
#include "common_util.h"
#include "contable.h"
void SetPara::load_from_memory(){
    auto pc = PWIConstTable::single();
    auto dc = DWIConstTable::single();
    ui->e_Psetnum->setText(QString::number(pc->PSetNum));
    ui->e_ADCMax->setText(QString::number(dc->ADCMax));
    ui->e_Pslicenum->setText(QString::number(pc->PSliceNum));
    ui->e_TE->setText(QString::number(pc->TE));
    ui->e_TR->setText(QString::number(pc->TR));
    ui->e_Stdnum->setText(QString::number(pc->STDNum));
    ui->e_Pmask->setText(QString::number(pc->PMask));
    ui->e_ck1->setText(QString::number(pc->ck1));
    ui->e_ck2->setText(QString::number(pc->ck2));
    ui->e_ck3->setText(QString::number(pc->ck3));
    ui->e_TType->setText(QString::number(pc->TType));
    ui->e_Pr->setText(QString::number(pc->pr));
    ui->e_Ro->setText(QString::number(pc->ro));
    ui->e_HSV->setText(QString::number(pc->HSV));
    ui->e_HLV->setText(QString::number(pc->HLV));
    ui->e_Coef->setText(QString::number(pc->Coef));
    ui->e_CBFC->setText(QString::number(pc->CBFC));
    ui->e_MTTC->setText(QString::number(pc->MTTC));
    ui->e_Tmax_th->setText(QString::number(pc->Tmax_th));
    ui->e_Vp_th->setText(QString::number(pc->VP_th));
    ui->e_Dsetnum->setText(QString::number(dc->DSetNum));
    ui->e_Dslicenum->setText(QString::number(dc->DSliceNum));
    ui->e_b->setText(QString::number(dc->b));
    ui->e_ADCMin->setText(QString::number(dc->ADCMin));
    ui->e_ADCMax->setText(QString::number(dc->ADCMax));
    ui->e_ADC_th->setText(QString::number(dc->ADC_th));
    ui->e_VD_th->setText(QString::number(dc->VD_th));
}

void SetPara::update_memory(){
    auto pc = PWIConstTable::single();
    auto dc = DWIConstTable::single();
    pc -> PSetNum = ui->e_Psetnum->text().toInt();
    dc -> ADCMax = ui->e_ADCMax->text().toDouble();
    pc -> PSliceNum = ui ->  e_Pslicenum->text().toInt();
    pc -> TE = ui -> e_TE -> text().toInt();
    pc -> TR = ui -> e_TR -> text().toInt();
    pc -> STDNum = ui -> e_Stdnum->text().toInt();
    pc -> PMask = ui -> e_Pmask->text().toDouble();
    pc -> ck1 = ui -> e_ck1->text().toDouble();
    pc -> ck2 = ui -> e_ck2->text().toDouble();
    pc -> ck3 = ui -> e_ck3->text().toDouble();
    pc -> TType = ui -> e_TType->text().toInt();
    pc -> pr = ui -> e_Pr -> text().toDouble();
    pc -> ro = ui -> e_Ro -> text().toDouble();
    pc -> HSV = ui -> e_HSV -> text().toDouble();
    pc -> HLV = ui -> e_HLV -> text().toDouble();
    pc -> Coef = ui -> e_Coef -> text().toInt();
    pc -> CBFC = ui -> e_CBFC -> text().toInt();
    pc -> MTTC = ui -> e_MTTC -> text().toInt();
    pc -> Tmax_th = ui -> e_Tmax_th -> text().toInt();
    pc -> VP_th = ui -> e_Vp_th -> text().toInt();
    dc -> DSetNum = ui -> e_Dsetnum -> text().toInt();
    dc -> DSliceNum = ui -> e_Dslicenum -> text().toInt();
    dc -> b = ui -> e_b -> text().toInt();
    dc -> ADCMin = ui -> e_ADCMin -> text().toDouble();
    dc -> ADCMax = ui -> e_ADCMax -> text().toDouble();
    dc -> ADC_th = ui -> e_ADC_th -> text().toDouble();
    dc -> VD_th = ui -> e_VD_th -> text().toDouble();
}

SetPara::SetPara(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SetPara)
{
    ui->setupUi(this);
    vector <QObject *> res;
    arr_label.clear();
    this -> setMouseTracking(true);
    commutil::getallchildren(this, "QLabel", res);
    for(int i = 0; i < res.size(); ++i){
        auto label = (QLabel *)res[i];
        auto str = label -> text().toStdString();
        if(str == "DWI参数设置" || str == "PWI参数设置")
            continue;
        else
            arr_label.push_back(label);
    }
    connect(ui -> update_memory, SIGNAL(clicked()),this, SLOT(update_now()));
    connect(ui -> update_file, SIGNAL(clicked()), this, SLOT(update_file()));
    //设置 参数 的 描述显示
    explain.clear();
    explain["Psetnum"] = "PWI数据总数";
    label_type ["Psetnum"] = 0;
    explain["Pslicenum"] = "PWI名字每组数据切片数";
    label_type ["Pslicenum"] = 0;
    explain["TE"] = "PWI相邻切片获取时间间隔";
    label_type ["TE"] = 0;
    explain["TR"] = "PWI两套数据获取时间间隔";
    label_type ["TR"] = 0;
    explain["Stdnum"] = "获取灌注前标准图像的套数编号";
    label_type ["Stdnum"] = 0;
    explain["Pmask"] = "判断MASK的PWI信号值不应低于的百分比";
    label_type ["Pmask"] = 1;
    explain["ck1"] = "PWI处理第4步中的差异函数的第一个经验系数常数";
    label_type ["ck1"] = 1;
    explain["ck2"] = "PWI处理第4步中的差异函数的第二个经验系数常数";
    label_type ["ck2"] = 1;
    explain["ck3"] = "PWI处理第4步中的差异函数的第三个经验系数常数";
    label_type ["ck3"] = 1;
    explain["TType"] = "0表示1.5T型号，1表示3.0T型号";
    label_type ["TType"] = 0;
    explain["x"] = "PWI处理第7步中2种类型对应的3个常数";
    label_type ["x"] = 1;
    explain["Pr"] = "PWI处理第8步中，计算n用到的常数";
    label_type ["Pr"] = 1;
    explain["Ro"] = "计算cbv和cbf用到的常数";
    label_type ["Ro"] = 1;
    explain["HSV"] = "计算cbv和cbf用到的常数";
    label_type ["HSV"] = 1;
    explain["HLV"] = "计算cbv和cbf用到的常数";
    label_type ["HLV"] = 1;
    explain["Coef"] = "计算经验常熟K0时用到的100";
    label_type ["Coef"] = 0;
    explain["CBFC"] = "计算CBF时候用到的常数60";
    label_type ["CBFC"] = 0;
    explain["MTTC"] = "计算MTT时用到的常数60";
    label_type ["MTTC"] = 0;
    explain["Tmax_th"] = "判断PWI病灶时的阈值为6秒";
    label_type ["Tmax_th"] = 0;
    explain["Vp_th"] = "过滤VP的阈值为3ml";
    label_type ["Vp_th"] = 1;
    explain["Dsetnum"] = "DWI数据总数";
    label_type ["Dsetnum"] = 0;
    explain["Dslicenum"] = "DWI每套数据的切片数";
    label_type ["Dslicenum"] = 0;
    explain["b"] = "ADC常数";
    label_type ["b"] = 0;
    explain["ADCMin"] = "判断mask的adc有效范围";
    label_type ["ADCMin"] = 1;
    explain["ADCMax"] = "判断mask的adc有效范围";
    label_type ["ADCMax"] = 1;
    explain["ADC_th"] = "判断DWI病灶的ADC阈值";
    label_type ["ADC_th"] = 1;
    explain["VD_th"] = "过滤Vd的阈值为1ml";
    label_type ["VD_th"] = 1;
    QRegExp rx_int("^[0-9]{1,4}$");//这个10就是最大长度
    QRegExp rx_double("^(\-)?[0-9]{1,4}(\.)?[0-9]?[0-9]?[0-9]?[0-9]?$");
    res.clear();
    commutil::getallchildren(this, "QLineEdit", res);
    for(int i = 0; i < res.size(); ++i){
        auto label = (QLineEdit *)res[i];
        printf("%s\n",label -> objectName().toStdString().c_str());
        if(label_type[label -> objectName().toStdString().substr(2)]){
            QValidator * validator_double = new QRegExpValidator(rx_double, 0);
            label -> setValidator(validator_double);
        }
        else{
            QValidator * validator_int = new QRegExpValidator(rx_int,0);
            label -> setValidator(validator_int);
        }
    }
    //从memory 显示数据
    load_from_memory();
}

SetPara::~SetPara()
{
    delete ui;
}
void SetPara::mouseMoveEvent(QMouseEvent * event){
   int ex = event -> globalPos().x();
   int ey = event -> globalPos().y();
   printf("%d %d\n", ex, ey);
   for(int i = 0; i < arr_label.size(); ++i){
       auto txt = arr_label[i] -> text();
       auto p = arr_label[i] -> pos();
       int x = arr_label[i] -> mapToGlobal(QPoint(0,0)).x();
       int y = arr_label[i] -> mapToGlobal(QPoint(0,0)).y();
       int width = arr_label[i] -> size().width();
       int height = arr_label[i] -> size().height();
       if(ex >= x  && ex <= x + width  && ey >= y && ey <= y + height){
           ui -> hint -> setText((txt.toStdString() + " : " + explain[txt.toStdString()]).c_str());
       }
   }
}

void SetPara::update_now(){
    update_memory();
    QMessageBox::about(this,"",tr("更新成功"));
}

void SetPara::update_file(){
    update_memory();
    //update file
    PWIConstTable::single() -> dump();
    DWIConstTable::single() -> dump();
    QMessageBox::about(this,"",tr("更新默认成功"));
}
