#ifndef MAINUI_H
#define MAINUI_H

#include <QMainWindow>
#include <iostream>
#include "brain_ui.h"
#include "search_patient.h"
#include "show_img.h"
using namespace std;
namespace Ui {
class mainui;
}

class mainui : public QMainWindow
{
    Q_OBJECT

public:
    explicit mainui(QWidget *parent = 0, double _dwi = 0);
    ~mainui();
public slots:
    void setupbui(){
        if(bui == NULL){
            bui = new brain_ui();
            bui -> setWindowTitle("脑部诊断");
        }
        bui -> show();
        return;
    }
    void setupvui(){
        if(vui == NULL){
            vui = new search_patient("vessel");
            vui->setWindowTitle("心血管诊断");
        }
        vui -> show();
    }

private:
    Ui::mainui *ui;
    brain_ui * bui;
    search_patient * vui;
    //pne_ui * pui;
    //cvd_ui * cui;
};

#endif // MAINUI_H
