#ifndef SETPARA_H
#define SETPARA_H

#include <QWidget>
#include "QTSource.h"
#include "common_util.h"
namespace Ui {
class SetPara;
}

class SetPara : public QWidget
{
    Q_OBJECT

public:
    explicit SetPara(QWidget *parent = 0);
    ~SetPara();
    int Psetnum;
    int Pslicenum;
    int TE;
    int TR;
    int STDNum;
    double Pmask;
    double ck1;
    double ck2;
    double ck3;
    int TType;
    double x[2][3];
    double pr;
    double ro;
    double hsv;
    double hlv;
    int coef;
    int cbfc;
    int mttc;
    int Tmax_th;
    double VP_th;
    int Dsetnum;
    int Dslicenum;
    int b;
    double adcmin;
    double adcmax;
    double adc_th;
    double VD_th;
    void mouseMoveEvent(QMouseEvent * event);
    void load_from_memory();
    void update_memory();
public slots:
    void update_now();
    void update_file();
private:
    Ui::SetPara *ui;
    vector <QLabel *> arr_label;
    map <string, string> explain;//map val to hint
    map <string, int> label_type;
};

#endif // SETPARA_H
