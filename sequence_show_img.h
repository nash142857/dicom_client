#ifndef SEQUENCE_SHOW_IMG_H
#define SEQUENCE_SHOW_IMG_H

#include <QWidget>
#include "common_util.h"
namespace Ui {
class sequence_show_img;
}

class sequence_show_img : public QWidget
{
    Q_OBJECT

public:
    explicit sequence_show_img(QWidget *parent = 0);
    ~sequence_show_img();
    //设置图片数据，过滤数值，宽度，长度，图片idx
    void set_para(vector <int *> _datas, double _filter_value, int _idx, int _width, int _height){
        datas = _datas;
        filter_value = _filter_value;
        idx = _idx;
        width = _width;
        height = _height;
        zoom_radio = 1;//初始无缩放
    }

    void loadpic();
public slots:
    void last_show();
    void next_show();
    void redrawpic(int);//重新比例显示图片
private:
    Ui::sequence_show_img *ui;
    vector <int *> datas;
    double filter_value;
    int idx;
    int width;
    int height;
    QImage img;
    QPixmap mp;
    double zoom_radio;//缩放比例
};

#endif // SEQUENCE_SHOW_IMG_H
