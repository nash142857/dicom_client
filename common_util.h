#ifndef COMMON_H
#define COMMON_H
#include <iostream>
#include <string>
#include <vector>
#include "QTSource.h"
#include <cstring>
#include <string>
#include <fstream>
#include <QJsonObject>
#include <QJsonDocument>
using namespace std;
class IntWidget: public QTableWidgetItem{
public:
    IntWidget(const QString & text, int type = Type){
        QTableWidgetItem(text,type);
    }

    virtual bool operator<(const QTableWidgetItem &item) const{
        return text().toInt() < text().toInt();
    }
};

namespace commutil {
    void getallchildren(QWidget * root, const char * label, vector <QObject *> & res);
    //get a patient data root dir;
    string rootdir(string id);
    inline bool get_json_obj(const QByteArray & json_str, QJsonObject & res){
        QJsonParseError json_error;
        QJsonDocument document = QJsonDocument::fromJson(json_str, &json_error);
        if(json_error.error == QJsonParseError::NoError){
            if(document.isObject()){
                QJsonObject obj = document.object();
                res = obj;
                return true;
            }
        }
        return false;
    }

    inline bool get_json_arr(const QByteArray & json_str, QJsonArray & res){
        QJsonParseError json_error;
        QJsonDocument document = QJsonDocument::fromJson(json_str, &json_error);
        if(json_error.error == QJsonParseError::NoError){
            if(document.isArray()){
                QJsonArray obj = document.array();
                res = obj;
                return true;
            }
        }
        return false;
    }
    inline bool judge_temp_file(QString str){
        return str.startsWith("s_");
    }
    //auto load data to database
    void autoload(QString db_type, QString data_dir, QString subdir);
}


#endif // COMMON_H
