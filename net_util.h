#ifndef NET_UTIL_H
#define NET_UTIL_H
#include <QString>
#include <map>
using std::map;
namespace net_util{
    extern QString base_url;
    extern QString get_version_url;
    extern QString get_file_url;
    extern QString rename_version_url;
    extern QString delete_version_url;
    extern int tcp_port;
    extern QString tcp_host;
    bool get(QString url, QString & res);
    bool post(QString url, const map <QString, QString> & mp, QString & res);
}
#endif // NET_UTIL_H
