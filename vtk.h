#ifndef VTK_H
#define VTK_H
#include <QString>
#include "vtk/vtkPolyDataWriter.h"
#include "vtk/vtkSmartPointer.h"
#include "vtk/vtkRenderer.h"
#include "vtk/vtkRenderWindow.h"
#include "vtk/vtkRenderWindowInteractor.h"
#include <QProcess>
#include "vtk/vtkPolyDataReader.h"
#include "vtk/vtkPolyDataMapper.h"
#include "vtk/vtkCamera.h"
#include "vtk/vtkXMLPolyDataReader.h"
#include "vtk/vtkPolyDataMapper.h"
#include "vtk/vtkActor.h"
#include "vtk/vtkPolyData.h"
//some methods based on vtk files
class vtk
{
public:
    vtk();
    static void show_vtp_file(QString & file_path);
};

#endif // VTK_H
