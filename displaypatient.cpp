#include "displaypatient.h"
#include "ui_displaypatient.h"
#include "show_img.h"
#include "common_util.h"
#include "contable.h"
#include <map>
#include "net_util.h"
DisplayPatient::DisplayPatient(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DisplayPatient)
{
    ui->setupUi(this);
    resize(1000,600);
    //init NULL
    dwi_display = NULL;
    all_disp = NULL;
    scrollshow = NULL;//滑动显示all picture
    for(int i = 0; i < 4; ++i)
        other_pic_disp[i] = NULL;
    patient = NULL;
    show_label.push_back("CBV");
    show_label.push_back("CBF");
    show_label.push_back("MTT");
    show_label.push_back("Tmax");
    ui ->tab_img->addTab((dwi_display = new show_img()),"ADC");
    for(int i = 0; i < show_label.size(); ++i){
        ui ->tab_img->addTab((other_pic_disp[i] = new show_img()), show_label[i].c_str());
    }
    ui -> tab_img -> addTab(scrollshow = new QScrollArea() ,"整合图");
    all_disp = new show_img();
    connect(ui -> tab_img, SIGNAL(currentChanged(int)), this, SLOT(show_all(int)));
    //set delete once closed
    QWidget::setAttribute(Qt::WA_DeleteOnClose);
    connect(ui -> save, SIGNAL(clicked()), this, SLOT(save_data()));
}
void DisplayPatient::show_all(int i){
    if(i == 5){// 整合图
        int total = all_datas.size();
        vector <int *> datas;
        int pwidth = patient -> mPWIWidth;
        int pheight = patient -> mPWIHeight;
        for(int i = 0; i < total; ++i){
            int * tmp = new int[pwidth * pheight * 3];
            for (int j = 0; j < pwidth * pheight * 3; ++j){
                tmp[j] = all_datas[i][j];
            }
            datas.push_back(tmp);
        }
        //show all pic with one picture
        show_img * img = new show_img();
        //img -> resize(240, 1200);
        img -> set_row_col(20, 4);
        //原始长宽比例
        int desktopheight = QApplication::desktop() -> screenGeometry().height();
        cout << desktopheight << endl;
        img -> resize(desktopheight * 4 * pwidth / (20 * pheight), desktopheight);
        img -> set_para(datas, pwidth, pheight);
        img -> setWindowTitle("整合图");
        QDesktopWidget* desktop = QApplication::desktop();
        img -> move((desktop->width() - img->width())/2, (desktop->height() - img->height())/2);
        img -> show();
    }
}
//with a para version and patient data id, we show a patient
void DisplayPatient::set_inf(DCMPatientInfo * data, string _version){
    version = _version;
    //hide save button if version is a old version
    if (!commutil::judge_temp_file(QString(_version.c_str()))){
        ui -> save -> setVisible(false);
        ui -> filename -> setVisible(false);
    }
    //显示PWI DWI 参数
    string id = data -> getid();
    //load table 进行显示
    ui -> pwi -> setText(PWIConstTable::display_pwi_para(commutil::rootdir(id)+ "/result/" + version + "/PWICONST").c_str());
    ui -> dwi -> setText(DWIConstTable::display_dwi_para(commutil::rootdir(id) + "/result/" + version + "/DWICONST").c_str());
    patient = data;
    int dwidth = patient -> mDWIWidth;
    int dheight = patient -> mDWIHeight;
    int pwidth = patient -> mPWIWidth;
    int pheight = patient -> mPWIHeight;
    auto dwi_val = patient -> getDWIVolumn();
    auto pwi_val = patient -> getPWIVolumn();
    //output DWI PWI
    //IMPORTANT STRCAT STACK OVERFLOW
    char s1[150],s2[50];
    memset(s1,0,sizeof(s1));
    memset(s2,0,sizeof(s2));
    sprintf(s1, "DWI 数值: %.2f,  PWI 数值: %.2f",dwi_val, pwi_val);
    //output mismtch, 如果dwi_val == 0 (没病) 输出None
    if(dwi_val == 0){
        sprintf(s2,", Mismatch 数值: None");
    }
    else{
        sprintf(s2,", Mismatch 数值: %.2f", pwi_val / dwi_val);
    }
    //产生最终输出
    strcat(s1, s2);
    ui -> result->setText(QString(s1));
    auto dwi_data = patient ->get_dwi_display();
    dwi_display -> set_para(dwi_data,dwidth,dheight);
    //data to be shown and pointer to be released
    int total;
    for(int i = 0; i < show_label.size(); ++i){
        QDir dir((commutil::rootdir(id) + "/result/" + string(version) + "/bmp" + show_label[i]).c_str());
        cout << commutil::rootdir(id) + "/result/" + string(version) + "/bmp" + show_label[i] << endl;
        dir.setFilter(QDir::Files);
        QFileInfoList list = dir.entryInfoList();
        total = list.count();
        vector <int *> datas(total, NULL);
        printf("%d\n", total);
        int height, width;
        for(int j = 0; j < total; ++j){
            QFileInfo it = list.at(j);
            auto filename = it.fileName().toStdString();
            int idx = filename.find("_");
            //if not find, so file error
            if(idx == -1){
                printf("find file error\n");
            }
            else{
                QImage* img= new QImage;
                int num = atoi(filename.substr(0,idx).c_str());
                img->load((it.absoluteFilePath().toStdString()).c_str());
                height = img -> height();
                width = img -> width();
                unsigned char * bits = img -> bits();
                int * data =  new int[width * height * 3];
                for (int i = 0; i < height; ++i)
                    for(int j = 0; j < width; ++j){
                        unsigned char r = *(bits + 2);
                        unsigned char g = *(bits + 1);
                        unsigned char b = *bits;
                        int idx = i * width + j;
                        data [3 * idx] = b;
                        data [3 * idx + 1] = g;
                        data [3 * idx + 2] = r;
                        bits += 4;
                    }
                datas[num] = data;
            }
        }
        other_pic_disp[i] -> set_para(datas, width, height);
    }
    int row = 20;
    int col = show_label.size();
    for(int i = 0; i < row; ++i)
        for(int j = 0; j < col; ++j){
            int * data = new int[pwidth * pheight * 3];
            for(int k = 0; k < pwidth * pheight * 3; ++k)
                //copypic
                data[k] = other_pic_disp[j]->datas[i][k];
            all_datas.push_back(data);
        }
    vector <int *> datas;
    for(int i = 0; i < all_datas.size(); ++i){
        int * tmp = new int[pwidth * pheight * 3];
        for (int j = 0; j < pwidth * pheight * 3; ++j){
            tmp[j] = all_datas[i][j];
        }
        datas.push_back(tmp);
    }
    all_disp -> set_row_col(20, 4);
    all_disp -> set_para(datas, pwidth, pheight);
    all_disp -> resize(4 * pwidth, 20 * pheight);
    scrollshow -> setWidget(all_disp);
}
DisplayPatient::~DisplayPatient()
{
    //析构的时候 delete 内部对象
    if(patient != NULL)delete patient;
    if(dwi_display != NULL)delete dwi_display;
    //cbv cbf mtt tmax all 4 pictures
    for(int i = 0; i < 4; ++i){
        if(other_pic_disp[i] != NULL) delete other_pic_disp[i];
    }
    if(all_disp != NULL) delete all_disp;
    for(int i = 0; i < all_datas.size(); ++i)
        delete all_datas[i];
    delete ui;
}

//保存 计算了的一份数据,(通过filename 控件 输入来指定保存文件名)
void DisplayPatient::save_data(){
    if(commutil::judge_temp_file(ui->filename->text())){
        QMessageBox::about(this,"",tr("请以s_开头版本号"));
        return;
    }
    auto id = patient->getid();
    //保存 计算后的图片
    QDir dir((commutil::rootdir(id) + "/result").c_str());
    //modify dir to the save filename to save data
    QDir exist_dir((commutil::rootdir(id) + "/result/" + ui->filename->text().toStdString()).c_str());
    if(exist_dir.exists()){
        QMessageBox::about(this,"",tr("版本存在，保存失败"));
        return;
    }
    QString res;
    //update remote server
    std::map<QString,QString> mp;
    mp["old_version_name"] = version.c_str();
    mp["new_version_name"] = ui->filename->text();
    mp["pid"] = id.c_str();
    bool rename_flg = net_util::post(net_util::rename_version_url, mp, res);
    if(!rename_flg){
        QMessageBox::about(this,"",tr("网络故障，保存失败"));
        return;
    }
    QJsonObject obj;
    bool get_json_flg = commutil::get_json_obj(res.toUtf8(),obj);
    if(!get_json_flg){
        QMessageBox::about(this,"",tr("服务故障，保存失败"));
        return;
    }
    if(obj.take("status").toString() != "success"){
        QMessageBox::about(this,"",tr("版本存在，保存失败"));
        return;
    }
    //net_util::rename_version_url
    dir.rename(version.c_str(), ui -> filename -> text());
    QMessageBox::about(this,"",tr("保存成功"));
}

//CBV CBF MTT Tmaxd
