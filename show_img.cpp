#include "show_img.h"
#include "ui_show_img.h"
#include "sequence_show_img.h"
#include "QsLog.h"
const double no_filter_display_radio = -1;//calculated DWI PWI Display

//show a full-image,a signal to -> show each single image

show_img::show_img(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::show_img)
{
    ui->setupUi(this);
    filter_value_radio = no_filter_display_radio;
    num_per_col = 5;
    num_per_row = 4;
}

show_img::~show_img()
{
    cout << "delete img data" << endl;
    for(int i = 0; i < datas.size(); ++i)
        delete [] datas[i];
    delete ui;
}
void show_img::paintEvent(QPaintEvent *){
    QLOG_DEBUG() << "paint event" << this << endl;
    QPainter painter(this);
    img = QImage(num_per_col * widths,num_per_row * heights, QImage::Format_RGB32);
    int total = num_per_col * heights * num_per_row * widths;
    for(int i = 0; i < datas.size(); ++ i){
        int row = i / num_per_col;
        int col = i % num_per_col;
        for(int j = 0; j < heights; ++j)
            for(int k = 0; k < widths; ++k){
                int num = j * widths + k;
                if(k + col * widths < num_per_col * widths && j + row * heights < num_per_row * heights);
                else
                    continue;
                if(filter_value_radio == no_filter_display_radio){
                    //no filter show
                    img.setPixel(k + col * widths,j + row * heights,qRgb(datas[i][3 * num + 2],datas[i][3 * num + 1],datas[i][3 * num + 0]));
                }
                else{
                    // filter show
                    if(datas[i][3 * num + 2] < int(filter_value_radio * 255)){
                        img.setPixel(k + col * widths,j + row * heights,qRgb(0,0,0));//black
                    }
                    else{
                        img.setPixel(k + col * widths,j + row * heights,qRgb(datas[i][3 * num + 2],datas[i][3 * num + 1],datas[i][3 * num + 0]));
                    }
                }
            }
    }
    int k = datas.size();
    k *= (widths * heights);
    //fill the rest pixel
    for(;k < total; ++k){
        int row = k / (num_per_col * widths);
        int col = k % (num_per_col * widths);
        //printf("%d %d\n",row, col);
        img.setPixel(col, row, qRgb(0,0,0));
    }
    QRect rect(0, 0, width(), height());
    painter.drawImage(rect,img);
}
void show_img::mouseDoubleClickEvent(QMouseEvent * pos){
    int ex = pos->x();
    int ey = pos->y();
    int col = ex / (width() / num_per_col);
    int row = ey / (height() / num_per_row);
    //with data and filter radio, we sequencely show pic from number
    sequence_show_img * ssi =  new sequence_show_img();
    ssi -> set_para(datas, filter_value_radio, row * num_per_col + col, widths, heights);
    //load pic before show
    ssi -> loadpic();
    ssi -> show();
    cout << "end mouse click" << endl;
}
