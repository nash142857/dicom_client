#ifndef BRAIN_UI_H
#define BRAIN_UI_H

#include <QWidget>
#include <setpara.h>
#include "search_patient.h"
#include <displaypwi.h>
namespace Ui {
class brain_ui;
}

class brain_ui : public QWidget
{
    Q_OBJECT

public:
    explicit brain_ui(QWidget *parent = 0);
    ~brain_ui();
    DisplayPWI * displaypwi;
    search_patient * patient_search;
    SetPara * set_pa;
public slots:
    void load(int i);
private:
    Ui::brain_ui *ui;
};

#endif // BRAIN_UI_H
