#include <cstdio>

#include "QTSource.h"
#include "mainui.h"
#include "dcmpatientinfo.h"
#include "imagematrix.h"
#include <memory>
#include <iostream>
#include "datacon.h"
#include "net_util.h"
#include "config.h"
#include "QsLog.h"
#include <QString>
extern const char * create_plist;
using namespace std;
//create database
//use attacker;
/*create table plist (pid varchar(255), pname varchar(255), pdir varchar(255), dwicount int,
                      pwicount int, modifytime date); */
void createdb(){
    QSqlDatabase db = QSqlDatabase::addDatabase(Config::single()->mp["db_type"]);
    db.setDatabaseName(Config::single()->mp["db_name"]);
    if (!db.open()){
      puts("no");
      qDebug() << "Failed to connect to root mysql admin";
    }
    QSqlQuery q;
    std::cout << q.exec(create_plist) << std::endl;
    std::cout << q.exec("insert into plist values('test', 'Danny','asdasd','asd')") << std::endl;

}

void testdb(){
    QSqlDatabase db = QSqlDatabase::addDatabase(Config::single()->mp["db_type"]);
    db.setDatabaseName(Config::single()->mp["db_name"]);
    if (!db.open()){
      puts("no");
      qDebug() << "Failed to connect to root mysql admin";
    }
    QSqlQuery q;
    q.exec("select pid,pname,checktime, importtime from brain_plist");
    while(q.next()){
        string id = q.value(0).toString().toStdString();
        string name = q.value(1).toString().toStdString();
        string time1 = q.value(2).toString().toStdString();
        string time2 = q.value(3).toString().toStdString();
        cout << id << " " << name << " " << time1 << " " << time2 << endl;
    }

}
void clean(QString table_name){
    QSqlDatabase db = QSqlDatabase::addDatabase(Config::single()->mp["db_type"]);
    db.setDatabaseName(Config::single()->mp["db_name"]);
    if (!db.open()){
      puts("no");
      qDebug() << "Failed to connect to root mysql admin";
    }
    QSqlQuery q;
    q.exec("delete from " + table_name);
    cout << "clear database done" << endl;
    //q.exec(create_plist);
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    mainui w(NULL);
    //init config
    auto config = Config::single();
    //init log
    QsLogging::Logger & log = QsLogging::Logger::instance();
    log.setLoggingLevel(QsLogging::DebugLevel);
    log.addDestination(QsLogging::DestinationFactory::MakeFileDestination(config -> mp["log_file"]));
    /*
    clean(config->mp["brain_db_table"]);
    clean(config->mp["vessel_db_table"]);
    return 0;
    */
    //init db
    DataCon::init();
    DataCon::insert("brain", config->mp["brain_db_table"].toStdString());
    DataCon::insert("vessel", config->mp["vessel_db_table"].toStdString());
    //createdb();
    //testdb()
    //自动载入数据
    commutil::autoload("brain","brain_pdata_dir", "brain_sub_dir");
    commutil::autoload("vessel","vessel_pdata_dir", "vessel_sub_dir");
    //clean(config->mp["vessel_db_table"]);
    //clean(config->mp["brain_db_table"]);
    w.show();
    int ret_code = a.exec();
    //remove database connection
    QSqlDatabase::removeDatabase("default");
    return ret_code;
}
