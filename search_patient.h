#ifndef BRAIN_ONLINE_H
#define BRAIN_ONLINE_H
#include "QTSource.h"
#include <QWidget>
#include "datacon.h"
#include "common_util.h"
#include "displaypatient.h"
namespace Ui {
class search_patient;
}

class search_patient : public QWidget
{
    Q_OBJECT

public:
    explicit search_patient(const QString & _data_type, QWidget *parent = 0);
    ~search_patient();
public slots:
    void sort(int idx);
    void search();

private:
    Ui::search_patient * ui;
    QString data_type;
    void setdata(vector <Patient_information> & data);
};
#endif // BRAIN_ONLINE_H
