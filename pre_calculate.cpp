#include "pre_calculate.h"
#include "ui_pre_calculate.h"
#include "dcmpatientinfo.h"
#include "displaypatient.h"
#include <QNetworkAccessManager>
#include <iostream>
#include <QNetworkRequest>
#include <QUrl>
#include <QJsonObject>
#include <QFileInfo>
#include <qfileinfo.h>
#include <QJsonDocument>
#include "net_util.h"
#include "config.h"
#include <QProcess>
#include "QsLog.h"
#include "common_util.h"
#include "contable.h"
#include <QTcpSocket>
#include <QHostAddress>
pre_calculate::pre_calculate(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::pre_calculate){

}
pre_calculate::pre_calculate(QString _id):QWidget(0),ui(new Ui::pre_calculate), id(_id),down_file(NULL){
    ui->setupUi(this);
    QWidget::setAttribute(Qt::WA_DeleteOnClose);//delete when close
    model = new QStringListModel(this);
    ui -> listView -> setEditTriggers(QAbstractItemView::NoEditTriggers);
    model -> setStringList(list);
    ui -> listView -> setModel(model);
    connect(ui -> calculate, SIGNAL(clicked()), this,SLOT(cal()));
    connect(ui -> select , SIGNAL(clicked()), this,SLOT(select_result()));
    connect(ui -> del, SIGNAL(clicked()), this,SLOT(delete_result()));
    connect(ui -> stop_calculate, SIGNAL(clicked()),this,SLOT(stop_cal()));
    ui->stop_calculate->setEnabled(false);
    get_file_list();
}

//重新计算
void pre_calculate::cal(){
    ui->select->setEnabled(false);
    ui->calculate->setEnabled(false);
    ui->stop_calculate->setEnabled(true);
    ui->del->setEnabled(false);
    std::string pwi_para_str = PWIConstTable::single()->dump();
    std::string dwi_para_str = DWIConstTable::single()->dump();
    QJsonObject json;
    json.insert("pwi_para", pwi_para_str.c_str());
    json.insert("dwi_para", dwi_para_str.c_str());
    json.insert("pid",id.toStdString().c_str());
    QJsonDocument document;
    document.setObject(json);
    QEventLoop loop;
    if(client.isOpen()){
        client.close();
    }
    QHostAddress addr(net_util::tcp_host);
    client.connectToHost(addr,net_util::tcp_port);
    connect(&client, SIGNAL(connected()), &loop, SLOT(quit()));
    connect(&client,SIGNAL(error(QAbstractSocket::SocketError)),&loop,SLOT(quit()));
    loop.exec();
    if(!client.isValid()){
        return;
    }
    client.write(document.toJson(QJsonDocument::Compact));
    connect(&client, SIGNAL(readyRead()), this, SLOT(deal_tcp_input()));
    ui->msg->setText("正在请求远程计算");
}
//选择 已经计算过的版本进行显示
void pre_calculate::select_result(){
    ui->del->setEnabled(false);
    ui->calculate->setEnabled(false);
    int row_num = ui -> listView -> currentIndex().row();
    if(row_num < 0 || row_num > list.size()){
        //not select a valid row
        return;
    }
    version = list.at(row_num);
    //check data if exists in local
    QDir dir(Config::single()->mp["brain_pdata_dir"] + "/" + id + "/result/" + version);
    if(!dir.exists()){
        //download
        down_load_file();
    }
    else{
        //load local and display
        display();
    }
}

void pre_calculate::write_local_file(){
    cout << "write local" << endl;
    down_file -> write(reply->readAll());
}

pre_calculate::~pre_calculate()
{
    delete ui;
}
//删除版本数据
void pre_calculate::delete_result(){
    int row_num = ui -> listView -> currentIndex().row();
    if(row_num < 0 || row_num >= list.size()){
        return;
    }
    string version = list.at(row_num).toStdString();
    //get result dir using ID and version
    string result_data_dir = commutil::rootdir(id.toStdString()) + "/result/" + version;
    //delete remote data
    std::map<QString,QString>mp;
    mp["version_name"] = version.c_str();
    mp["pid"] = id;
    QString res;
    bool delete_flg = net_util::post(net_util::delete_version_url, mp,res);
    if(!delete_flg){
        QMessageBox::about(this,"",tr("网络故障,删除失败"));
    }
    QJsonObject obj;
    bool get_json_flg = commutil::get_json_obj(res.toUtf8(),obj);
    if(!get_json_flg){
        QMessageBox::about(this,"",tr("服务故障，保存失败"));
        return;
    }
    if(obj.take("status").toString() != "success"){
        QMessageBox::about(this,"",tr("版本不存在,删除失败"));
        return;
    }
    QDir dir(result_data_dir.c_str());
    if(!dir.exists()){
        return;
    }
    //delete local data dir
    dir.removeRecursively();
    //delete data str in list model
    model -> removeRow(ui -> listView -> currentIndex().row());
}

void pre_calculate::update_list_version(){
    QJsonObject obj;
    bool flg = commutil::get_json_obj(reply->readAll(),obj);
    if (!flg) return;
    if(obj.contains("status")){
        if(obj.take("status").toString() == "success"){
            QString version_list = obj.take("content").toString();
            printf("%s\n",version_list.toStdString().c_str());
            QJsonArray json_arr;
            flg = commutil::get_json_arr(version_list.toUtf8(),json_arr);
            if(!flg) return;
            list.clear();
            foreach (QJsonValue val, json_arr) {
                list << val.toString();
            }
            model->setStringList(list);
            ui->msg->setText("当前已计算版本");
            return;
        }
        else{
            return;
        }
    }
    else{
        return;
    }
}
void pre_calculate::trans_file_done(){
    cout << "trains done id: " << id.toStdString() << " " << "version: " << version.toStdString() << endl;
    if(down_file){
        down_file->close();
        delete down_file;
        down_file = NULL;
    }
    if(reply->error() == QNetworkReply::NoError){
        ui->msg->setText("数据传输完成，正在跳转");
        //解压缩文件 完成跳转
        QProcess process;
        process.setWorkingDirectory(Config::single() -> mp["brain_pdata_dir"] + "/" + id + "/result");
        cout << "unzip " + version.toStdString() + ".zip " << endl;
        process.start("unzip " + version + ".zip ");
        bool ret_code = process.waitForFinished();
        if(!ret_code){
            ui->msg->setText("数据解析失败");
            return;
        }
        //跳转
        display();
    }
    else{
        ui->msg->setText("网络故障,获取失败");
    }
}
void pre_calculate::network_failure(){
    cout << "net work failure" << endl;
    ui->msg->setText("网络故障,获取失败");
    if(down_file){
        down_file->close();
        delete down_file;
        down_file = NULL;
    }
}
//get version
void pre_calculate::get_file_list(){
    ui ->msg ->setText("正在获取计算版本");
    reply = net_access.get(QNetworkRequest(QUrl(net_util::get_version_url + "?pid=" + id)));
    connect(reply, SIGNAL(readyRead()),this, SLOT(update_list_version()));
    connect(reply, SIGNAL(finished()), reply, SLOT(deleteLater()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(network_failure()));
}

void pre_calculate::down_load_file(){
    //下载文件
    cout << "download file" << endl;
    ui->msg->setText("正在从服务器获取数据");
    QNetworkRequest req;
    QEventLoop loop;
    req.setUrl(QUrl(net_util::get_file_url +  "?pid=" + id + "&version_name=" + version));
    //redirect
    reply = net_access.get(req);
    connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    connect(reply, SIGNAL(finished()), reply,SLOT(deleteLater()));
    loop.exec();
    QString redirect_file_url = reply->rawHeader("Location");
    //init file to save file
    QFileInfo fileinfo(redirect_file_url);
    std::cout << "debug file name" << fileinfo.fileName().toStdString() << std::endl;
    QNetworkRequest redirect_req;
    std::cout << "redirect url" << redirect_file_url.toStdString() << std::endl;
    redirect_req.setUrl(QUrl(net_util::base_url + redirect_file_url));
    //IMPORTANT 需要设置文件头
    redirect_req.setRawHeader("Content-Type","application/octet-stream");
    reply = net_access.get(redirect_req);
    if(reply->error() != QNetworkReply::NoError){
        ui->msg->setText("internal error");
        return;
    }
    //设置写文件
    if(down_file != NULL){
        down_file->close();
        delete down_file;
        down_file = NULL;
    }
    down_file = new QFile(Config::single()->mp["brain_pdata_dir"] + "/" + id + "/result/" + version + ".zip");
    QLOG_DEBUG() << "debug: file"<<Config::single()->mp["brain_pdata_dir"] + "/" + id + "/result/" + version + ".zip";
    if(!down_file->open(QIODevice::WriteOnly)){
        delete down_file;
        down_file = NULL;
        QLOG_ERROR() << "open file write error: " << Config::single()->mp["brain_pdata_dir"] + "/" + id + "/result/" + version + ".zip";
        return;
    }
    //传输文件内容
    cout << "begin connect" << endl;
    connect(reply, SIGNAL(readyRead()),this, SLOT(write_local_file()));
    cout << "begin connect" << endl;
    connect(reply, SIGNAL(readChannelFinished()), this, SLOT(trans_file_done()));
    cout << "begin connect" << endl;
    connect(reply, SIGNAL(finished()), reply, SLOT(deleteLater()));
    cout << "begin connect" << endl;
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(network_failure()));
    cout << "begin connect" << endl;
}
void pre_calculate::display(){
    cout << "display:" << id.toStdString() << " " << version.toStdString() << endl;
    auto disp = new DisplayPatient();
    DCMPatientInfo * patient;
    try{
        patient = DCMPatientInfo::get_patient_info(id.toStdString(), version.toStdString());
    }
    catch(...){
        ui->msg->setText("数据计算失败，请重新计算");
        return;
    }

    disp->setWindowTitle(("ID: " + id.toStdString() + " , Version: " + version.toStdString()).c_str());
    disp -> set_inf(patient, version.toStdString());
    disp->show();
}


void pre_calculate::deal_tcp_input(){
    QDataStream in(&client);
    int l = client.bytesAvailable();
    char  tmp[100];
    memset(tmp, 0, sizeof(tmp));
    in.readRawData(tmp,l);
    string str(tmp);
    QJsonObject res;
    bool flg = commutil::get_json_obj(str.c_str(),res);
    if(!flg){
        return;
    }
    cout << "deal str:" << str << endl;
    QString status_desc = res.take("status").toString();
    if(status_desc == "start"){
        ui->msg->setText("正在远程计算");
    }
    if(status_desc == "available"){
        client.close();
        id = res.take("pid").toString();
        version = res.take("version").toString();
        ui->select->setEnabled(true);
        ui->del->setEnabled(true);
        ui->calculate->setEnabled(true);
        ui->stop_calculate->setEnabled(false);
        down_load_file();
    }
    cout << "deal tcp input done" << endl;
}

void pre_calculate::stop_cal(){
    client.write("stop");
    client.flush();
    client.close();
    ui->del->setEnabled(true);
    ui->calculate->setEnabled(true);
    ui->select->setEnabled(true);
    ui->stop_calculate->setEnabled(false);
    ui->msg->setText("停止请求");
}
