#ifndef   CONSTABLE_H
#define   CONSTABLE_H
#include "common_util.h"
#include "config.h"
#include <sstream>
class PWIConstTable
{
public:
    int PSetNum;
    int PSliceNum;
	int TE;
	int TR;
	int STDNum;
    double PMask;
	float ck1,ck2,ck3;
	int TType;
	double x[2][3];
	double pr;
	double ro,HSV,HLV;
	int Coef;
	int CBFC;
	int MTTC;
	int Tmax_th;
    int VP_th;
    bool load(string default_pos = "/PWICONST"){
        string file_pos = Config::single()->mp["brain_pdata_dir"].toStdString() + default_pos;
        ifstream fin(file_pos.c_str());
        if(!fin.is_open()){
            //open file error
            printf("%d open %s error\n", file_pos.c_str());
            return false;
        }
        fin >> PSetNum >> PSliceNum >> TE >> TR >> STDNum >> PMask >> ck1 >> ck2 >> ck3 >> TType;
        for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 3; ++j)
                fin >> x[i][j];
        fin >> pr >> ro >> HSV >> HLV >> Coef >> CBFC >> MTTC >> Tmax_th >> VP_th;
        fin.close();
        return true;
    }
    string dump(){
        ostringstream out;
        out << PSetNum << " " << PSliceNum << " " << TE<< " "  << TR << " " << STDNum << " "  << PMask << " " << ck1 << " " << ck2 << " " << ck3 << " "  << TType << " " ;
        for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 3; ++j)
                out << x[i][j] << " ";
        out << pr << " " << ro << " " << HSV << " " << HLV << " "  << Coef << " " << CBFC << " " << MTTC << " " << Tmax_th << " "  << VP_th << endl;
        return out.str();
    }
    string tostring(){
       string format = "PWI:\n\tPSetNum = %d\n\tPSliceNum = %d\n\tTE = %d TR = %d \n\tSTDNum = %d\n\tPMask = %.1f\n\tck1, ck2, ck3 = %.1f, %.1f, %.1f\n\tTType = %d\n\tpr = %.2f\n\tro, HSV,HLV = %.2f,%.2f,%.2f\n\tCoef = %d CBFC = %d    MTTC = %d\n";
       char res[1000];
       memset(res, 0, sizeof(res));
       sprintf(res, format.c_str(), PSetNum, PSliceNum, TE, TR, STDNum, PMask,ck1, ck2, ck3, TType, pr, ro, HSV, HLV, Coef, CBFC, MTTC);
       return string(res);
    }
    static PWIConstTable * single(){
        static PWIConstTable * singler = NULL;
        if(singler == NULL){
            singler =  new PWIConstTable();
            if(!singler -> load()){
                singler -> dump();
            }
        }
        return singler;
    }

    static string display_pwi_para(string filename){
        int PSetNum;
        int PSliceNum;
        int TE;
        int TR;
        int STDNum;
        double PMask;
        float ck1,ck2,ck3;
        int TType;
        double x[2][3];
        double pr;
        double ro,HSV,HLV;
        int Coef;
        int CBFC;
        int MTTC;
        int Tmax_th;
        int VP_th;
        ifstream fin(filename.c_str());
        if(!fin.is_open()){
            //open file error
            printf("%s open %s error\n", filename.c_str());
            return "";
        }
        fin >> PSetNum >> PSliceNum >> TE >> TR >> STDNum >> PMask >> ck1 >> ck2 >> ck3 >> TType;
        for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 3; ++j)
                fin >> x[i][j];
        fin >> pr >> ro >> HSV >> HLV >> Coef >> CBFC >> MTTC >> Tmax_th >> VP_th;
        fin.close();
        string format = "PWI:\n\tPSetNum = %d\n\tPSliceNum = %d\n\tTE = %d TR = %d \n\tSTDNum = %d\n\tPMask = %.1f\n\tck1, ck2, ck3 = %.1f, %.1f, %.1f\n\tTType = %d\n\tpr = %.2f\n\tro, HSV,HLV = %.2f,%.2f,%.2f\n\tCoef = %d CBFC = %d    MTTC = %d\n";
        char res[1000];
        memset(res, 0, sizeof(res));
        sprintf(res, format.c_str(), PSetNum, PSliceNum, TE, TR, STDNum, PMask,ck1, ck2, ck3, TType, pr, ro, HSV, HLV, Coef, CBFC, MTTC);
        return string(res);
    }

    //PWI参数:\n     PSetNum = 60\n     PSliceNum = 22\n     TE =   TR = \n     STDNum = 4\n     PMask = 0.5\n     ck1, ck2, ck3 = 1.0, -3.5, -1.0\n     TType = 0\n    x[2][3]信息\n     pr = 0.15\n     rp, HSV,HLV = 1.04,0.25,0.45\n     Coef = 100     CBFC = 60\n     MTTC = 60\n
    //DWI常数:\n     DSetNum = 2\n     DSliceNum = 25\n     b = 1000\n     ADCMin = 0.0011\n     ADCMax = 0.0006\n     VD_th = 1\n
private:
    PWIConstTable();
};
class DWIConstTable
{
public:
	int DSetNum;
	int DSliceNum;
	int b;
	double ADCMin;
	double ADCMax;
	double ADC_th;
    double VD_th;
    bool load(string default_pos = "/DWICONST"){
        string file_pos = Config::single()->mp["brain_pdata_dir"].toStdString() + default_pos;
        ifstream fin(file_pos.c_str());
        if(!fin.is_open()){
            //open file error
            printf("%s open %s error\n", file_pos.c_str());
            return false;
        }
        fin >> DSetNum >> DSliceNum >> b >> ADCMin >> ADCMax >> ADC_th >> VD_th;
        fin.close();
        return true;
    }
    static string display_dwi_para(string filename){
        int DSetNum;
        int DSliceNum;
        int b;
        double ADCMin;
        double ADCMax;
        double ADC_th;
        double VD_th;
        ifstream fin(filename.c_str());
        if(!fin.is_open()){
            //open file error
            printf("%s open %s error\n", filename.c_str());
            return "";
        }
        fin >> DSetNum >> DSliceNum >> b >> ADCMin >> ADCMax >> ADC_th >> VD_th;
        fin.close();
        string format = "DWI:\n\tDSetNum = %d\n\tDSliceNum = %d\n\tb = %d\n\tADCMin = %.4f\n\tADCMax = %.4f\n\tVD_th = %.2f\n";
        char res[1000];
        memset(res, 0, sizeof(res));
        sprintf(res, format.c_str(), DSetNum, DSliceNum, b, ADCMin, ADCMax, VD_th);
        return string(res);
    }

    string dump(){
        ostringstream out;
        out << DSetNum <<  " " << DSliceNum <<  " " << b <<  " " << ADCMin <<  " " << ADCMax <<  " " << ADC_th <<  " " << VD_th << endl;
        return out.str();
    }

    string tostring(){
        string format = "DWI:\n\tDSetNum = %d\n\tDSliceNum = %d\n\tb = %d\n\tADCMin = %.4f\n\tADCMax = %.4f\n\tVD_th = %.2f\n";
        char res[1000];
        memset(res, 0, sizeof(res));
        sprintf(res, format.c_str(), DSetNum, DSliceNum, b, ADCMin, ADCMax, VD_th);
        return string(res);
    }
    static DWIConstTable * single(){
        static DWIConstTable * singler = NULL;
        if(singler == NULL){
            singler =  new DWIConstTable();
            //使用 默认的全局const 初始化单例
            if(!singler -> load()){
                singler -> dump();
            }
        }
        return singler;
    }
private:
    DWIConstTable();
};
#endif
