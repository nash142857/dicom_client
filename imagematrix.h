#ifndef IMAGEMATRIX_H
#define IMAGEMATRIX_H
#include "DCMConst.h"
#include "DCMHeaders.h"
#include "common_util.h"
#include <string>
using std::string;
class ImageMatrix
{
private:
    string patientid;
    string patientname;
    Uint16 mRows;
    Uint16 mColumns;
    //数据源指针
    Uint16 *mPixData16;
    Uint8 *mPixData8;
    double mWindowCenter;
    double mWindowWidth;
    int mPixLength;
    bool kind;
public:
    ImageMatrix(void);
    ImageMatrix(DcmDataset *dataset);
    ~ImageMatrix(void);
    string getname(){
        return patientname;
    }
    string getid(){
        return patientid;
    }
    Uint16 getRows();
    Uint16 getColumns();
    double getWinCenter();
    double getWinWidth();
    static std::string get_patientname(const std::string &);//get a patient names given dir
    static std::string get_patientid(const std::string &);//get a patient id given id
    //得到[row,column]处的pixel data
    //get pixel data of the position
    Uint8 getUint8Pixel(int row, int column);
    Uint16 getUint16Pixel(int row, int column);
    void setUint16Pixel(int row, int column, Uint16 pixelValue);
    //得到数据指针
    Uint8* getUint8Data();
    Uint16* getUint16Data();
    string mContentTime;
    string mTestInfo;
    Uint8 mIndex;
    Float64 pixelSpacing;//每个像素之间的实际距离 单位是毫米
    Float64 sliceThickness;//切片厚度
    Float32 PWICoe;//一个PWI系数
    int * display(int judgevalue = 10000, int lexcept = 0, int rexcept = 3000){
        const Uint16 * p = getUint16Data();
        int width = mRows;
        int height = mColumns;
        int * data = new int[width * height * 3];
        int minvalue = -1, maxvalue = -1;
        for(int i = 0; i < height * width; ++i){
            if(p[i] < lexcept || p[i] > rexcept) // delete
                continue;
            if(minvalue == -1 || p[i] < minvalue)
                minvalue = p[i];
            if(maxvalue == -1 || p[i] > maxvalue)
                maxvalue = p[i];
        }
        int rangevalue = maxvalue - minvalue;
        for(int i = 0; i < height * width; ++i){
            int reverse_i = i;//(height - 1 - (i / width)) * width + (i % width);
            if(p[reverse_i] == judgevalue){
                data[3 * i] = 255;
                data[3 * i + 1] = data[3 * i + 2] = 0;
            }
            else
                if(p[reverse_i] > rexcept){
                    data[3 * i] = data[3 * i + 1] = data[3 * i + 2] = 0;
                }
            else{
                //the bmp pic is reverse
                data[3 * i] = data[3 * i + 1] = data[3 * i + 2] = int(1.0 * 255 * (p[reverse_i] - minvalue) / rangevalue);
                //cout << "f:" << data[3 * i] << endl;
            }
        }
        return data;
    }

};
#endif // IMAGEMATRIX_H
