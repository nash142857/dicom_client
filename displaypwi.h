#ifndef DISPLAYPWI_H
#define DISPLAYPWI_H

#include <QWidget>
#include "QTSource.h"
#include "common_util.h"
#include "imagematrix.h"
namespace Ui {
class DisplayPWI;
}

class DisplayPWI : public QWidget
{
    Q_OBJECT

public:
    explicit DisplayPWI(QWidget *parent = 0);
    ~DisplayPWI();
public slots:
    void display();
    void changeshow(int value);
    void savepic();
private:
    Ui::DisplayPWI *ui;
    vector <ImageMatrix *> pwi_list;
    QString lastshow;
};

#endif // DISPLAYPWI_H
