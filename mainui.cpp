#include "mainui.h"
#include "ui_mainui.h"
#include "config.h"
mainui::mainui(QWidget *parent, double _dwi) :
    QMainWindow(parent),
    ui(new Ui::mainui)
{
    bui = NULL;
    ui->setupUi(this);
    connect(ui->brain, SIGNAL(clicked()), this, SLOT(setupbui()));
    connect(ui->pne, SIGNAL(clicked()), this, SLOT(setupvui()));
    QImage img;
    img.load((Config::single()->mp["base_dir"].toStdString() + "/body.jpg").c_str());
    img = img.scaled(ui -> pic -> width(),ui -> pic -> height());
    ui -> pic -> setPixmap(QPixmap::fromImage(img));
}

mainui::~mainui()
{
    qDebug() << "close mainui";
    if(bui != NULL)delete bui;
    if(vui != NULL)delete vui;
    delete ui;
}
