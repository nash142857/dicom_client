#include "common_util.h"
#include "config.h"
#include "imagematrix.h"
#include "datacon.h"
#include "QsLog.h"
const char * create_plist ="create table plist(pid varchar(255), pname varchar(255), checktime varchar(255), importtime varchar(255));";
void commutil::getallchildren(QWidget * root, const char * label, vector <QObject *> & res){
    auto list = root -> children();
    foreach(QObject * obj, list){
        if(strcmp(obj -> metaObject() -> className(), label) == 0){
            res.push_back(obj);
        }
        else
            if(strcmp(obj -> metaObject() -> className(), "QWidget") == 0){
                auto widget = (QWidget *)obj;
                getallchildren(widget, label, res);
            }
    }
}

string commutil::rootdir(string id){
    return Config::single()->mp["brain_pdata_dir"].toStdString() + "/" + id;
}

void commutil::autoload(QString db_type, QString data_dir, QString subdir){
    QDir dir(Config::single()->mp[data_dir]);
    dir.setFilter(QDir::Dirs);
    QFileInfoList list = dir.entryInfoList();
    int total = list.count();
    QDateTime lastdate;
    QFile f(Config::single()->mp[data_dir] +  "/modify.txt");
    if(!f.open(QIODevice::ReadOnly)){
        QLOG_ERROR() << "open:" << Config::single()->mp[data_dir] + "/modify.txt" << " error";
        return;
    }

    QDataStream stream(&f);
    stream >> lastdate;
    f.close();
    //prepare database
    for(int i = 0; i < total; ++i){
      QFileInfo it = list.at(i);
      auto filename = it.fileName();
      if(filename == "." || filename == ".."){
          continue;
      }
      auto date = it.lastModified();
      //may debug
      if(date > lastdate){
        auto patient_name = ImageMatrix::get_patientname((Config::single()->mp[data_dir] + "/" + filename + "/" + Config::single()->mp[subdir]).toStdString());
        //bug in dcmtk lib
        int bug_pos = patient_name.find(":bah:");
        if(bug_pos != -1){
            patient_name = patient_name.substr(0,bug_pos);
        }
        auto patient_id = ImageMatrix::get_patientid((Config::single()->mp[data_dir] + "/" + filename + "/" + Config::single()->mp[subdir]).toStdString());
        DataCon::getinstance(db_type.toStdString()) -> add(patient_id.c_str(), patient_name.c_str(),date.toString(), date.toString());
      }
    }
    //update check time
    lastdate = QDateTime::currentDateTime();
    if(!f.open(QIODevice::WriteOnly)){
        QLOG_ERROR() << "open:" << Config::single()->mp[data_dir] + "/modify.txt" << " error";
        return;
    }
    stream.setDevice(&f);
    stream << lastdate;
    f.close();
}
