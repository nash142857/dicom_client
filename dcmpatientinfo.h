#ifndef DCMPATIENTINFO_H
#define DCMPATIENTINFO_H
#include "DCMConst.h"
#include "DCMHeaders.h"
#include "ImageMatrix.h"
#include "common_util.h"
#include "datacon.h"
#include <vector>
using std::vector;
extern const char * temp_filename;
class DCMPatientInfo
{
private:
    string mPatientID;
    string mPatientName;
    double mPWIVolumn;
    double mDWIVolumn;
    int mDWINum;
    int mPWINum;
    int getIndex(int imageIndex, int row, int column, int type);//得出一个点对应整体的编号
    void floodFill(int imageIndex, int row, int column, int *count, int type, vector <int> & res);//在这个点BFS
    bool checkADC(int index);
    bool checkPWI(int index);
    bool check(int index, int type);
    bool volumnCompare(Float64 unitVolumn, int pixelCount, int type);
    DCMPatientInfo(void);
    void cutpoint(vector <int> & vt, int type);
    int cutadcfloor();//cut the discrete point
    int cutpointnum;
    int maxadc;
public:
    string getid(){
        return mPatientID;
    }
    ImageMatrix** mDWIImageList;
    ImageMatrix** mPWIImageList;
    ~DCMPatientInfo(void);
    void resetconfig(int _cutpointnum = 0, int _maxadc = 600){
        cutpointnum = _cutpointnum;
        maxadc = _maxadc;
        mPWIVolumn = mDWIVolumn = -1;
    }

    DCMPatientInfo(vector <DcmDataset*> DWIDataSet, vector <DcmDataset*> PWIDataSet);
    string getID(){
        return mPatientID;
    }
    string getPatientname(){
        return mPatientName;
    }
    double getDWIVolumn();//得到DWI的值
    double getPWIVolumn();//得到PWI的值
    int mDWIWidth;
    int mDWIHeight;
    int mPWIWidth;
    int mPWIHeight;
    vector < int * > get_dwi_display(){
        vector < int * > res;
        printf("dwinum:%d\n", mDWINum);
        for(int  i = 0; i < mDWINum; ++i){
            res.push_back(mDWIImageList[i] -> display());
        }
        return res;
    }
    vector < int * > get_pwi_display(){
        vector < int * > res;
        for(int  i = 0; i < mPWINum; ++i){
            res.push_back(mPWIImageList[i] -> display());
        }
        return res;
    }
    static DCMPatientInfo * get_patient_info(string id, string version = temp_filename);

};

#endif // DCMPATIENTINFO_H
