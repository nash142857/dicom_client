#ifndef show_img_H
#define show_img_H

#include <QWidget>
#include "common_util.h"

//Include showing for RGB and Filter_Value

namespace Ui {
class show_img;
}

class show_img : public QWidget
{
    Q_OBJECT

public:
    explicit show_img(QWidget *parent = 0);
    ~show_img();
    const int truewidth = 130;
    const int trueheight = 110;
    int num_per_col;
    int num_per_row;
    void set_para(vector <int *> _data, int _width, int _height){
        //delete old datas
        for(int i = 0; i < datas.size(); ++i)
            delete datas[i];
        datas = _data;
        heights = _height;
        widths = _width;
    }
    void paintEvent(QPaintEvent *);
    void set_filter_value_radio(double _filter_value_radio){
        filter_value_radio = _filter_value_radio;
    }
    void set_row_col(int _row, int _col){
        num_per_row = _row;
        num_per_col = _col;
    }

    vector <int *> datas;
    QImage img;
protected:
    virtual void mouseDoubleClickEvent(QMouseEvent *);
private:
    Ui::show_img *ui;
    int widths;
    int heights;
    double filter_value_radio;
};

#endif // show_img_H
