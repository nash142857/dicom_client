#-------------------------------------------------
#
# Project created by QtCreator 2014-10-17T16:46:44
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dicom

QMAKE_CXXFLAGS += -std=c++0x

TEMPLATE = app

SOURCES += main.cpp \
    net_util.cpp \
    BMP.cpp \
    brain_ui.cpp \
    contable.cpp \
    datacon.cpp \
    dcmpatientinfo.cpp \
    displaypatient.cpp \
    displaypwi.cpp \
    imagematrix.cpp \
    mainui.cpp \
    pre_calculate.cpp \
    sequence_show_img.cpp \
    setpara.cpp \
    show_img.cpp \
    vtk.cpp \
    search_patient.cpp \
    common_util.cpp

HEADERS += BMP.h \
    datacon.h \
    dcmpatientinfo.h \
    imagematrix.h \
    net_util.h \
    QTSource.h \
    brain_ui.h \
    config.h \
    contable.h \
    DCMConst.h \
    DCMHeaders.h \
    displaypatient.h \
    displaypwi.h \
    mainui.h \
    pre_calculate.h \
    sequence_show_img.h \
    setpara.h \
    show_img.h \
    vtk.h \
    search_patient.h \
    common_util.h

LIBS += -lz /Users/gsl/dcmtk/lib/libcharls.a \
    /Users/gsl/dcmtk/lib/libdcmdata.a \
    /Users/gsl/dcmtk/lib/libdcmdsig.a \
    /Users/gsl/dcmtk/lib/libdcmimage.a \
    /Users/gsl/dcmtk/lib/libdcmimgle.a \
    /Users/gsl/dcmtk/lib/libdcmjpeg.a \
    /Users/gsl/dcmtk/lib/libdcmjpls.a \
    /Users/gsl/dcmtk/lib/libdcmnet.a \
    /Users/gsl/dcmtk/lib/libdcmpstat.a \
    /Users/gsl/dcmtk/lib/libdcmqrdb.a \
    /Users/gsl/dcmtk/lib/libdcmsr.a \
    /Users/gsl/dcmtk/lib/libdcmtls.a \
    /Users/gsl/dcmtk/lib/libdcmwlm.a \
    /Users/gsl/dcmtk/lib/libijg8.a \
    /Users/gsl/dcmtk/lib/libijg12.a \
    /Users/gsl/dcmtk/lib/libijg16.a \
    /Users/gsl/dcmtk/lib/liblibi2d.a \
    /Users/gsl/dcmtk/lib/liboflog.a \
    /Users/gsl/dcmtk/lib/libofstd.a \
    /usr/local/lib/libfftw3f.a \
    /usr/local/lib/libfftw3.a \
    /usr/local/lib/libvtkChartsCore-6.2.1.dylib \
    /usr/local/lib/libvtkChartsCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkChartsCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonColor-6.2.1.dylib \
    /usr/local/lib/libvtkCommonColorPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonColorTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonComputationalGeometry-6.2.1.dylib \
    /usr/local/lib/libvtkCommonComputationalGeometryPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonComputationalGeometryTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonCore-6.2.1.dylib \
    /usr/local/lib/libvtkCommonCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonDataModel-6.2.1.dylib \
    /usr/local/lib/libvtkCommonDataModelPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonDataModelTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonExecutionModel-6.2.1.dylib \
    /usr/local/lib/libvtkCommonExecutionModelPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonExecutionModelTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonMath-6.2.1.dylib \
    /usr/local/lib/libvtkCommonMathPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonMathTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonMisc-6.2.1.dylib \
    /usr/local/lib/libvtkCommonMiscPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonMiscTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonSystem-6.2.1.dylib \
    /usr/local/lib/libvtkCommonSystemPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonSystemTCL-6.2.1.dylib \
    /usr/local/lib/libvtkCommonTransforms-6.2.1.dylib \
    /usr/local/lib/libvtkCommonTransformsPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkCommonTransformsTCL-6.2.1.dylib \
    /usr/local/lib/libvtkDICOMParser-6.2.1.dylib \
    /usr/local/lib/libvtkDomainsChemistry-6.2.1.dylib \
    /usr/local/lib/libvtkDomainsChemistryPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkDomainsChemistryTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersAMR-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersAMRPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersAMRTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersCore-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersExtraction-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersExtractionPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersExtractionTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersFlowPaths-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersFlowPathsPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersFlowPathsTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGeneral-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGeneralPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGeneralTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGeneric-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGenericPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGenericTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGeometry-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGeometryPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersGeometryTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersHybrid-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersHybridPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersHybridTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersHyperTree-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersHyperTreePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersHyperTreeTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersImaging-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersImagingPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersImagingTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersModeling-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersModelingPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersModelingTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersParallel-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersParallelImaging-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersParallelImagingPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersParallelImagingTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersParallelPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersParallelTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersProgrammable-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersProgrammablePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersProgrammableTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersPython-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersPythonPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSMP-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSMPPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSMPTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSelection-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSelectionPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSelectionTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSources-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSourcesPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersSourcesTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersStatistics-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersStatisticsPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersStatisticsTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersTexture-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersTexturePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersTextureTCL-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersVerdict-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersVerdictPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkFiltersVerdictTCL-6.2.1.dylib \
    /usr/local/lib/libvtkGeovisCore-6.2.1.dylib \
    /usr/local/lib/libvtkGeovisCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkGeovisCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOAMR-6.2.1.dylib \
    /usr/local/lib/libvtkIOAMRPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOAMRTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOCore-6.2.1.dylib \
    /usr/local/lib/libvtkIOCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOEnSight-6.2.1.dylib \
    /usr/local/lib/libvtkIOEnSightPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOEnSightTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOExodus-6.2.1.dylib \
    /usr/local/lib/libvtkIOExodusPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOExodusTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOExport-6.2.1.dylib \
    /usr/local/lib/libvtkIOExportPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOExportTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOGeometry-6.2.1.dylib \
    /usr/local/lib/libvtkIOGeometryPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOGeometryTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOImage-6.2.1.dylib \
    /usr/local/lib/libvtkIOImagePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOImageTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOImport-6.2.1.dylib \
    /usr/local/lib/libvtkIOImportPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOImportTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOInfovis-6.2.1.dylib \
    /usr/local/lib/libvtkIOInfovisPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOInfovisTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOLSDyna-6.2.1.dylib \
    /usr/local/lib/libvtkIOLSDynaPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOLSDynaTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOLegacy-6.2.1.dylib \
    /usr/local/lib/libvtkIOLegacyPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOLegacyTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOMINC-6.2.1.dylib \
    /usr/local/lib/libvtkIOMINCPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOMINCTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOMovie-6.2.1.dylib \
    /usr/local/lib/libvtkIOMoviePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOMovieTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIONetCDF-6.2.1.dylib \
    /usr/local/lib/libvtkIONetCDFPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIONetCDFTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOPLY-6.2.1.dylib \
    /usr/local/lib/libvtkIOPLYPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOPLYTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOParallel-6.2.1.dylib \
    /usr/local/lib/libvtkIOParallelPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOParallelTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOParallelXML-6.2.1.dylib \
    /usr/local/lib/libvtkIOParallelXMLPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOParallelXMLTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOSQL-6.2.1.dylib \
    /usr/local/lib/libvtkIOSQLPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOSQLTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOVideo-6.2.1.dylib \
    /usr/local/lib/libvtkIOVideoPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOVideoTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOXML-6.2.1.dylib \
    /usr/local/lib/libvtkIOXMLParser-6.2.1.dylib \
    /usr/local/lib/libvtkIOXMLParserPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOXMLParserTCL-6.2.1.dylib \
    /usr/local/lib/libvtkIOXMLPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkIOXMLTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingColor-6.2.1.dylib \
    /usr/local/lib/libvtkImagingColorPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingColorTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingCore-6.2.1.dylib \
    /usr/local/lib/libvtkImagingCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingFourier-6.2.1.dylib \
    /usr/local/lib/libvtkImagingFourierPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingFourierTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingGeneral-6.2.1.dylib \
    /usr/local/lib/libvtkImagingGeneralPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingGeneralTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingHybrid-6.2.1.dylib \
    /usr/local/lib/libvtkImagingHybridPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingHybridTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingMath-6.2.1.dylib \
    /usr/local/lib/libvtkImagingMathPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingMathTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingMorphological-6.2.1.dylib \
    /usr/local/lib/libvtkImagingMorphologicalPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingMorphologicalTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingSources-6.2.1.dylib \
    /usr/local/lib/libvtkImagingSourcesPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingSourcesTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingStatistics-6.2.1.dylib \
    /usr/local/lib/libvtkImagingStatisticsPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingStatisticsTCL-6.2.1.dylib \
    /usr/local/lib/libvtkImagingStencil-6.2.1.dylib \
    /usr/local/lib/libvtkImagingStencilPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkImagingStencilTCL-6.2.1.dylib \
    /usr/local/lib/libvtkInfovisCore-6.2.1.dylib \
    /usr/local/lib/libvtkInfovisCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkInfovisCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkInfovisLayout-6.2.1.dylib \
    /usr/local/lib/libvtkInfovisLayoutPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkInfovisLayoutTCL-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionImage-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionImagePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionImageTCL-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionStyle-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionStylePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionStyleTCL-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionWidgets-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionWidgetsPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkInteractionWidgetsTCL-6.2.1.dylib \
    /usr/local/lib/libvtkNetCDF-6.2.1.dylib \
    /usr/local/lib/libvtkNetCDF_cxx-6.2.1.dylib \
    /usr/local/lib/libvtkParallelCore-6.2.1.dylib \
    /usr/local/lib/libvtkParallelCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkParallelCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingAnnotation-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingAnnotationPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingAnnotationTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingContext2D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingContext2DPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingContextIIDTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingContextOpenGL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingContextOpenGLPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingContextOpenGLTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingCore-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingFreeType-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingFreeTypeOpenGL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingFreeTypeOpenGLPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingFreeTypeOpenGLTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingFreeTypePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingFreeTypeTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingGL2PS-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingGL2PSPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingGLtoPSTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingImage-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingImagePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingImageTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLIC-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLICPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLICTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLOD-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLODPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLODTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLabel-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLabelPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingLabelTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingOpenGL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingOpenGLPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingOpenGLTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingVolume-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingVolumeOpenGL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingVolumeOpenGLPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingVolumeOpenGLTCL-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingVolumePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkRenderingVolumeTCL-6.2.1.dylib \
    /usr/local/lib/libvtkViewsContext2D-6.2.1.dylib \
    /usr/local/lib/libvtkViewsContext2DPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkViewsContextIIDTCL-6.2.1.dylib \
    /usr/local/lib/libvtkViewsCore-6.2.1.dylib \
    /usr/local/lib/libvtkViewsCorePython27D-6.2.1.dylib \
    /usr/local/lib/libvtkViewsCoreTCL-6.2.1.dylib \
    /usr/local/lib/libvtkViewsInfovis-6.2.1.dylib \
    /usr/local/lib/libvtkViewsInfovisPython27D-6.2.1.dylib \
    /usr/local/lib/libvtkViewsInfovisTCL-6.2.1.dylib \
    /usr/local/lib/libvtkWrappingPython27Core-6.2.1.dylib \
    /usr/local/lib/libvtkalglib-6.2.1.dylib \
    /usr/local/lib/libvtkexoIIc-6.2.1.dylib \
    /usr/local/lib/libvtkexpat-6.2.1.dylib \
    /usr/local/lib/libvtkfreetype-6.2.1.dylib \
    /usr/local/lib/libvtkftgl-6.2.1.dylib \
    /usr/local/lib/libvtkgl2ps-6.2.1.dylib \
    /usr/local/lib/libvtkhdf5-6.2.1.dylib \
    /usr/local/lib/libvtkhdf5_hl-6.2.1.dylib \
    /usr/local/lib/libvtkjpeg-6.2.1.dylib \
    /usr/local/lib/libvtkjsoncpp-6.2.1.dylib \
    /usr/local/lib/libvtklibxml2-6.2.1.dylib \
    /usr/local/lib/libvtkmetaio-6.2.1.dylib \
    /usr/local/lib/libvtkoggtheora-6.2.1.dylib \
    /usr/local/lib/libvtkpng-6.2.1.dylib \
    /usr/local/lib/libvtkproj4-6.2.1.dylib \
    /usr/local/lib/libvtksqlite-6.2.1.dylib \
    /usr/local/lib/libvtksys-6.2.1.dylib \
    /usr/local/lib/libvtktiff-6.2.1.dylib \
    /usr/local/lib/libvtkverdict-6.2.1.dylib \
    /usr/local/lib/libvtkzlib-6.2.1.dylib

include(qt-components/QsLog/QsLog.pri)
INCLUDEPATH += /Users/gsl/dcmtk/include/
INCLUDEPATH += /usr/local/include/

OTHER_FILES += data/*
OTHER_FILES += conf/*
OTHER_FILES += log/*

FORMS    += mainui.ui \
    brain_ui.ui \
    brain_offline.ui \
    displaypatient.ui \
    displaypwi.ui \
    setpara.ui \
    show_img.ui \
    sequence_show_img.ui \
    pre_calculate.ui \
    search_patient.ui
