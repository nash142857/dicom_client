#ifndef PRE_CALCULATE_H
#define PRE_CALCULATE_H

#include <QWidget>
#include "QTSource.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
namespace Ui {
class pre_calculate;
}

class pre_calculate : public QWidget
{
    Q_OBJECT

public:
    explicit pre_calculate(QWidget *parent = 0);
    explicit pre_calculate(QString id);
    ~pre_calculate();
    QString id;//id is used to resolve patient data
    QString version;
public slots:
    void cal();
    void select_result();
    void delete_result();
    void update_list_version();
    void write_local_file();
    void trans_file_done();
    void network_failure();
    void deal_tcp_input();
    void stop_cal();
private:
    Ui::pre_calculate *ui;
    //show all version name
    QStringListModel * model;
    QStringList list;// all version name string list
    QNetworkAccessManager net_access;
    QNetworkReply * reply;
    QFile * down_file;
    QTcpSocket client;
    //refresh to sync server data
    void get_file_list();
    void down_load_file();
    void display();
};

#endif // PRE_CALCULATE_H
